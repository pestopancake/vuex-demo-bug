import { createStore } from 'vuex'
import layout from "./layout";
import { LayoutStateInterface } from "./layout";

export interface StateInterface {
  layout: LayoutStateInterface;
}

export default createStore<StateInterface>({
  modules: {
    layout
  }
})
