import { Module } from "vuex";
import { StateInterface } from "./index";

export interface LayoutStateInterface {
  leftDrawerOpen: boolean;
  rightDrawerOpen: boolean;
}

export enum GetterTypes {
  GET_LEFT_DRAWER_OPEN = "GET_LEFT_DRAWER_OPEN",
  GET_RIGHT_DRAWER_OPEN = "GET_RIGHT_DRAWER_OPEN"
}

export enum ActionTypes {
  MODIFY_LEFT_DRAWER_OPEN = "MODIFY_LEFT_DRAWER_OPEN",
  MODIFY_RIGHT_DRAWER_OPEN = "MODIFY_RIGHT_DRAWER_OPEN",
}

export enum MutationTypes {
  MUTATE_LEFT_DRAWER_OPEN = "MUTATE_LEFT_DRAWER_OPEN",
  MUTATE_RIGHT_DRAWER_OPEN = "MUTATE_RIGHT_DRAWER_OPEN",
}

const layoutModule: Module<LayoutStateInterface, StateInterface> = {
  namespaced: true,
  actions: {
    [ActionTypes.MODIFY_LEFT_DRAWER_OPEN]: (context) => {
      void context.commit(MutationTypes.MUTATE_LEFT_DRAWER_OPEN);
    },
    [ActionTypes.MODIFY_RIGHT_DRAWER_OPEN]: (context) => {
      void context.commit(MutationTypes.MUTATE_RIGHT_DRAWER_OPEN);
    }
  },
  getters: {
    [GetterTypes.GET_LEFT_DRAWER_OPEN]: (state) => {
      return state.leftDrawerOpen;
    },
    [GetterTypes.GET_RIGHT_DRAWER_OPEN]: (state) => {
      return state.rightDrawerOpen;
    }
  },
  mutations: {
    [MutationTypes.MUTATE_LEFT_DRAWER_OPEN]: (state: LayoutStateInterface) => {
      state.leftDrawerOpen = !state.leftDrawerOpen;
    },
    [MutationTypes.MUTATE_RIGHT_DRAWER_OPEN]: (state: LayoutStateInterface) => {
      state.rightDrawerOpen = !state.rightDrawerOpen;
    }
  },
  state: {
    leftDrawerOpen: false,
    rightDrawerOpen: false
  }
};

export default layoutModule;